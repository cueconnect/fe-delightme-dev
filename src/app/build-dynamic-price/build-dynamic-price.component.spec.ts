import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildDynamicPriceComponent } from './build-dynamic-price.component';

describe('BuildDynamicPriceComponent', () => {
  let component: BuildDynamicPriceComponent;
  let fixture: ComponentFixture<BuildDynamicPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildDynamicPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildDynamicPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
