import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BuildDynamicPriceComponent } from './build-dynamic-price.component';
import { SharedModule } from '../shared';

const buildDynamicPriceHome: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'BuildDynamicPrice/Home',
    component: BuildDynamicPriceComponent
  }
]);

@NgModule({
  imports: [
    buildDynamicPriceHome,
    SharedModule
  ],
  declarations: [
    BuildDynamicPriceComponent
  ],
  providers: [
  ]
})
export class BuildDynamicPriceHomeModule {}
