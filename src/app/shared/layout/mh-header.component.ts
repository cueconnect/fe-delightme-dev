import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'layout-mh-header',
  templateUrl: './mh-header.component.html',
  styleUrls: ['./mh-header.component.scss'],
  host: {
    '(document:click)': 'onBodyClick()',
  }
})
export class MHHeaderComponent implements OnInit {

  constructor() {}

  today: number = Date.now();
  isDropdownVisible: boolean = false;
  dropdownsItems: Object[] = [
    { title: 'Store Account', path: '/customize-expirence/store-account' },
    { title: 'Pricing & Billing', path: '/customize-expirence/priceAndBillings' },
    { title: 'Help', path: '/customize-expirence/help' }
  ];

  onBodyClick(): void {
    this.isDropdownVisible = false;
  }

  onDropdownClick(event): void {
    event.stopPropagation();
  }

  signOut(): void {
    // @TO-DO: implement the signing out functionality
    alert('The user is signed out (@TO-DO: implement the signing out functionality)');
  }

  ngOnInit() {}

}
