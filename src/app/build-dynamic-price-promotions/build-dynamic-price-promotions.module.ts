import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BuildDynamicPricePromotionsComponent } from './build-dynamic-price-promotions.component';
import { SharedModule } from '../shared';

const buildDynamicPricePromotions: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'BuildDynamicPrice/Promotions',
    component: BuildDynamicPricePromotionsComponent
  }
]);

@NgModule({
  imports: [
    buildDynamicPricePromotions,
    SharedModule
  ],
  declarations: [
    BuildDynamicPricePromotionsComponent
  ],
  providers: [
  ]
})
export class BuildDynamicPricePromotionsModule {}
