import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildDynamicPricePromotionsComponent } from './build-dynamic-price-promotions.component';

describe('BuildDynamicPricePromotionsComponent', () => {
  let component: BuildDynamicPricePromotionsComponent;
  let fixture: ComponentFixture<BuildDynamicPricePromotionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildDynamicPricePromotionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildDynamicPricePromotionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
