import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService} from '../api.service';


@Component({
  selector: 'app-build-dynamic-price-promotions',
  templateUrl: './build-dynamic-price-promotions.component.html',
  styleUrls: ['./build-dynamic-price-promotions.component.css']
})
export class BuildDynamicPricePromotionsComponent implements OnInit {
  isActiveDiscount:boolean = false;
  discountList: any;
  constructor(
  	private apiService: ApiService,
  	private router:Router
  	) { }

  ngOnInit() {
  }

  activateDiscountEnbale(value:any){
  	let data = {
  		discountId:value,
  		isEnable:true
  	}
    this.apiService.put('/discount/enable??discount_id=',data)
         .subscribe(result=>{
         
         })
  }

 // function for delete discount
    deleteDiscount(value:any){
  	let data = {
  		discountId:value
  	}
    this.apiService.put('',data)
         .subscribe(result=>{
         
         })
  }

// update function for discount
    updateDiscount(value:any){
  	let data = {
  		discountId:value
  	}
    this.apiService.put('',data)
         .subscribe(result=>{
         
         })
  }

  // launch discount is any discount present
    getDiscountList(){
    	let data = {
    		
    	}
       this.apiService.put('',data)
         .subscribe(result=>{
         this.discountList = result.list;
         })	
    }
}
