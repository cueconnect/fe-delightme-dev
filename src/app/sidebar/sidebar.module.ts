import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SidebarComponent } from './sidebar.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const sidebarRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'sidebar',
    component: SidebarComponent
  }
]);

@NgModule({
  imports: [
    sidebarRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    SidebarComponent
  ],
  providers: [
  ]
})
export class SidebarModule {}
