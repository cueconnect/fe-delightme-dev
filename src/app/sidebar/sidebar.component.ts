import { Component, OnInit } from '@angular/core';
import {FormsModule,FormGroup, FormControl,Validators,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { ColorPickerInterface } from '../shared/color-picker/color-picker.interface';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
	 sidebarWelcome: any;
	 sidebarMyprice: any;
	 sidebarPriceDrop: any;
	 welcomeShow:boolean= false;
	 myPriceShow:boolean = false;
	 priceDropShow:boolean = false;
	 storeConfig:any;
	 priceDropnotificationColor:string="";
	 priceDroptext:boolean=false;
	 mypricetext:boolean=false;
	 welcomemodal:boolean=false;
	 is_enabled:boolean = false;
	 shopId:string =  "qa-delightme";
	 ColorPickerInterface: ColorPickerInterface = {
	    color: '#999999'
	  }

	 welcomeModalValue:any = {
	 	title: '',
	 	button:'',
        copytext:''
	 }
	 myPriceModalValue:any = {
	 	title: '',
        copytext:''
	 }
	 priceDropModalValue:any = {
	 	heading: '',
        copytext:''
	 }

  constructor(
  	private formBuilder: FormBuilder,
  	private router: Router,
  	private apiService: ApiService
  	) {
    }

  ngOnInit() {

  	this.loadDefaultConfiguration(true);
     if(window.localStorage.getItem('shop_id')){
          this.shopId = window.localStorage.getItem('shop_id');
        }

  }

 back(id:number){
	 	if(id==1){
	 	 this.router.navigateByUrl('/launcher');
	 	}
	 	else if(id==2)
	 	{
	 	 this.welcome(true);		
	 	}
	 	else {
         this.myprice(true);
	 	}
	 }

 onSubmitForm(id:number,value:boolean){
 	 if(!value){
 	 	if(id==1){
          this.myprice(true);
	 	 } else if(id==2){ 	
	 	 this.pricedrop(true);	
	 	 } else{
             let welcomeValue = this.sidebarWelcome._value;
             let myPriceValue = this.sidebarMyprice._value;
             let priceDropValue = this.sidebarPriceDrop._value;
	 	 	let data = {
				    "shop_id": this.shopId,
				    "sidebar": {
				        "is_enabled": this.is_enabled,
				        "my_price": {
				            "bg_color": myPriceValue.backgroundColor,
				            "heading": myPriceValue.title,
				            "content": myPriceValue.copytext,
				            "active_color": myPriceValue.activeColor
				        },
				        "welcome": {
				            "button": welcomeValue.button,
				            "bg_color": welcomeValue.backgroundColor,
				            "title": welcomeValue.title,
				            "content": welcomeValue.copytext,
				            "active_color": welcomeValue.activeColor
				        },
				        "price_drop": {
				            "bg_color": priceDropValue.backgroundColor,
				            "heading": priceDropValue.heading,
				            "content": priceDropValue.copytext,
				            "active_color": priceDropValue.activeColor,
				            "notification_color": priceDropValue.notificationColor
				        }
				    }
				}
				this.apiService.put('/store/sidebar',data)
			         .subscribe(result=>{
			         	// alert(result.message);
		 	         this.router.navigateByUrl('/welcome-modal');	 	 	
			         	
			         })
	 	 }
 	 	}
 	 	else {
 	 		return false;
 	 	}
	 }

 onChangeCopyText(value:any , id:number){
 	if(id==1){
 		 this.welcomeModalValue.copytext = value;
 	}
 }

 onChangebutton(value:any){
 	this.welcomeModalValue.button = value;
 }
 onChangetitle(value:any , id:number){
 	if(id==1){
 		 this.welcomeModalValue.title = value;
 	}
 }
 onChangebgColorWelcome(value:any){
 	document.getElementById('delightme-sidebar-form-modal').style.backgroundColor = value;
 	// this.updateBgColor(value);
 }
 onChangebgColorMyPrice(value:any){
 	document.getElementById('delightme-sidebar-full-height').style.backgroundColor = value;
 	// this.updateBgColor(value);
 }


 onChangeActiveColorPriceDrop(value:any){
    document.getElementById('price-drop-checkout-button').style.backgroundColor = value;
    document.getElementById('priceDropheader').style.backgroundColor = value;
    // this.updateActiveColor(value);
 }
onChangeActiveColorMyPrice(value:any){
    document.getElementById('myprice-checkout-button').style.backgroundColor = value;
    document.getElementById('mypriceheader').style.backgroundColor = value;
    // this.updateActiveColor(value);
 }
  onChangeactiveColorWelcome(value:any){
    document.getElementById('delightme-sidebar-modal-subscribe-submit').style.backgroundColor = value;
    // this.updateActiveColor(value);
 }
 onChangenotificationColor(value:any){
     document.getElementById('price-drop-notification').style.backgroundColor = value;
     document.getElementById('price-dropped-message').style.color = value;
     document.getElementById('price-dropped-message').style.border = "solid 1px "+value;
    
 }


 onChangeTitleMyPrice(value:any){
 	this.myPriceModalValue.title = value;
 }
 onChangeCopytextMyPrice(value:any){
 	this.myPriceModalValue.copytext = value;
 }
 onChangePriceDropHeading(value:any){
 	this.priceDropModalValue.heading = value;
 }
 onChangePriceDropCopytext(value:any){
 	this.priceDropModalValue.copytext = value;
 }
 onChangeBgColorPriceDrop(value:any){ 	
 	document.getElementById('delightme-sidebar-full-height').style.backgroundColor = value;
 	// this.updateBgColor(value);
 }

welcome(option:boolean){
         this.welcomeShow=true;
         this.myPriceShow = false;
	     this.priceDropShow = false;
 		document.getElementById('welcomeId').classList.add('active');
 		document.getElementById('mypriceId').classList.remove('active');
 		document.getElementById('pricedropId').classList.remove('active');
 		this.loadDefaultWelcome(this.onChangebgColorWelcome,this.sidebarWelcome._value.backgroundColor,this.onChangeactiveColorWelcome,this.sidebarWelcome._value.activeColor,null,null);
}
myprice(option:boolean){
         this.welcomeShow=false;
         this.myPriceShow = true;
	     this.priceDropShow = false;
 		document.getElementById('welcomeId').classList.remove('active');
 		document.getElementById('mypriceId').classList.add('active');
 		document.getElementById('pricedropId').classList.remove('active');
 		this.loadDefaultWelcome(this.onChangebgColorMyPrice,this.sidebarMyprice._value.backgroundColor,this.onChangeActiveColorMyPrice,this.sidebarMyprice._value.activeColor,null,null);
}
pricedrop(option:boolean){
         this.welcomeShow=false;
         this.myPriceShow = false;
	     this.priceDropShow = true;
 		document.getElementById('welcomeId').classList.remove('active');
 		document.getElementById('mypriceId').classList.remove('active');
 		document.getElementById('pricedropId').classList.add('active');
 		this.loadDefaultWelcome(this.onChangeBgColorPriceDrop,this.sidebarPriceDrop._value.backgroundColor,this.onChangeActiveColorPriceDrop,this.sidebarPriceDrop._value.activeColor,this.onChangenotificationColor,this.sidebarPriceDrop._value.notificationColor);
}
pricetextsetting(){
	this.priceDroptext=!this.priceDroptext;
}
mypricetextsetting(){
	this.mypricetext=!this.mypricetext;
}
welcometextsetting(){
	this.welcomemodal=!this.welcomemodal;
}

loadDefaultConfiguration(value:boolean){

  		this.apiService.get('/store?shop_id='+this.shopId)
	  	.subscribe(result =>{		    
	  		
	  		window.localStorage.setItem('authToken',result.data[0].dm_auth_token);
	  		window.localStorage.setItem('shopToken',result.data[0].shop_token);
	  		
	  		this.storeConfig=result.data[0].shop_config.sidebar;
	  		this.is_enabled = this.storeConfig.is_enabled;
	  		let welcome = this.storeConfig.welcome;

	  		this.welcomeModalValue.title = welcome.title;
	  		this.welcomeModalValue.button = welcome.button;
	  		this.welcomeModalValue.copytext = welcome.content;
            let color = /^#[0-9a-f]{3}([0-9a-f]{3})?$/i;
	  		 this.sidebarWelcome = this.formBuilder.group({
		  	  'backgroundColor': [welcome.bg_color, [ Validators.required,Validators.pattern(color)]],
		  	  'activeColor': [welcome.active_color,[ Validators.required,Validators.pattern(color)]],
		      'title': [welcome.title,Validators.required],
		      'button' :  [welcome.button,[ Validators.required,Validators.maxLength(30)]],
		      'copytext' : [welcome.content, Validators.required]
		    });


            let my_price = this.storeConfig.my_price;

            this.myPriceModalValue.title = my_price.heading;
	  		this.myPriceModalValue.copytext = my_price.content;
		    this.sidebarMyprice = this.formBuilder.group({
		  	  'backgroundColor': [my_price.bg_color,[ Validators.required,Validators.pattern(color)]],
		  	  'activeColor': [my_price.active_color, [ Validators.required,Validators.pattern(color)]],
		      'title': [my_price.heading, Validators.required],
		      'copytext' : [my_price.content, Validators.required]
		    });
            
            let price_drop = this.storeConfig.price_drop;
            this.priceDropModalValue.heading = price_drop.heading;
	  		this.priceDropModalValue.copytext = price_drop.content;
	  		this.priceDropnotificationColor=price_drop.notification_color;
		    this.sidebarPriceDrop = this.formBuilder.group({
		  	  'backgroundColor': [price_drop.bg_color,[ Validators.required,Validators.pattern(color)]],
		  	  'activeColor': [price_drop.active_color, [ Validators.required,Validators.pattern(color)]],
		      'notificationColor': [price_drop.notification_color,[ Validators.required,Validators.pattern(color)]],
		      'heading' :  [price_drop.heading, Validators.required],
		      'copytext' : [price_drop.content, Validators.required]
		    });
             if(value){
		  	   this.welcomeShow = true;
		  	  }
		  	  if(this.welcomeShow){
		       this.loadDefaultWelcome(this.onChangebgColorWelcome,welcome.bg_color,this.onChangeactiveColorWelcome,welcome.active_color,null,null);

		  	  }
		  	  if(this.myPriceShow){
		  	  	this.loadDefaultWelcome(this.onChangebgColorMyPrice,this.sidebarMyprice._value.backgroundColor,this.onChangeActiveColorMyPrice,this.sidebarMyprice._value.activeColor,null,null);
		  	  }
		  	  if(this.priceDropShow){
                this.loadDefaultWelcome(this.onChangeBgColorPriceDrop,this.sidebarPriceDrop._value.backgroundColor,this.onChangeActiveColorPriceDrop,this.sidebarPriceDrop._value.activeColor,this.onChangenotificationColor,this.sidebarPriceDrop._value.notificationColor);
		  	  }

	  	// document.getElementById('delightme-sidebar-form-modal').style.backgroundColor = welcome.bg_color;

	  	})
}

loadDefaultWelcome(fBg:any,valueBg:string,fActive:any,valueAc:string,fNotification:any,VNotfication:string){
	setTimeout(function(){
	    fBg(valueBg);
	    fActive(valueAc);
	    if(VNotfication){
	    fNotification(VNotfication);
	    }
	},0)
}

updateBgColor(value:any){
    if(this.welcomeShow){
      this.sidebarMyprice.controls['backgroundColor'].setValue(value);
      this.sidebarPriceDrop.controls['backgroundColor'].setValue(value);
	  }
    if(this.myPriceShow){
	  	this.sidebarWelcome.controls['backgroundColor'].setValue(value);
        this.sidebarPriceDrop.controls['backgroundColor'].setValue(value);
	  }
	if(this.priceDropShow){
      this.sidebarWelcome.controls['backgroundColor'].setValue(value);
      this.sidebarMyprice.controls['backgroundColor'].setValue(value);
	  }
}

updateActiveColor(value:any){
    if(this.welcomeShow){
      this.sidebarMyprice.controls['activeColor'].setValue(value);
      this.sidebarPriceDrop.controls['activeColor'].setValue(value);
	  }
    if(this.myPriceShow){
	  	this.sidebarWelcome.controls['activeColor'].setValue(value);
        this.sidebarPriceDrop.controls['activeColor'].setValue(value);
	  }
	if(this.priceDropShow){
      this.sidebarWelcome.controls['activeColor'].setValue(value);
      this.sidebarMyprice.controls['activeColor'].setValue(value);
	  }
}

}
