import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostomizeExperienceLAuncherComponent } from './costomize-experience-launcher.component';

describe('CostomizeExperienceLAuncherComponent', () => {
  let component: CostomizeExperienceLAuncherComponent;
  let fixture: ComponentFixture<CostomizeExperienceLAuncherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostomizeExperienceLAuncherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostomizeExperienceLAuncherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
