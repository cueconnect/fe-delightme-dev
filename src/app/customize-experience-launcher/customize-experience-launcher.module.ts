import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomizeExperienceLAuncherComponent } from './customize-experience-launcher.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const customizeExperienceLauncherRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/launcher',
    component: CustomizeExperienceLAuncherComponent
  }
]);

@NgModule({
  imports: [
    customizeExperienceLauncherRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    CustomizeExperienceLAuncherComponent
  ],
  providers: [
  ]
})
export class CustomizeExperienceLauncherModule {}
