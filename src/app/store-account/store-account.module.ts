import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StoreAccountComponent } from './store-account.component';
import { SharedModule } from '../shared';

const storeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'store',
    component: StoreAccountComponent
  }
]);

@NgModule({
  imports: [
    storeRouting,
    SharedModule
  ],
  declarations: [
    StoreAccountComponent
  ],
  providers: [
  ]
})
export class storeModule {}
