import { Component, OnInit } from '@angular/core';
import {FormsModule,FormGroup, FormControl,Validators,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-store-account',
  templateUrl: './store-account.component.html',
  styleUrls: ['./store-account.component.scss']
})
export class StoreAccountComponent implements OnInit {
	userForm: any;
	selectedanimal:String = "Bottom Right";
	storeConfig:any;
	showForm:boolean = false;
	shopId:string = "qa-delightme";

  constructor(
  	private formBuilder: FormBuilder,
  	private router: Router,
  	private apiService: ApiService
  	) {
  }

	  ngOnInit() {

	  	if(window.localStorage.getItem('shop_id')){
          this.shopId = window.localStorage.getItem('shop_id');
        }	  	
    /*
      * Api service to getting store configurtion  
    */
      let Url = /[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;
      let emailValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  	this.apiService.get('/store?shop_id='+this.shopId)
	  	.subscribe(result =>{
	  		this.storeConfig=result.data[0].shop_info;
	  		window.localStorage.setItem('authToken',result.data[0].dm_auth_token);
	  		window.localStorage.setItem('shopToken',result.data[0].shop_token);
	  		this.userForm = this.formBuilder.group({
		  	  'storename': [this.storeConfig.name,[ Validators.required,Validators.required,Validators.maxLength(41)]],
		  	  'storeurl': [this.storeConfig.domain,[Validators.required,Validators.pattern(Url)]],
		      'name': [(this.storeConfig.shop_owner).split(" ")[0],[Validators.maxLength(40)]],
		      'lastname' :  [(this.storeConfig.shop_owner).split(" ")[1],[Validators.maxLength(40)]],
		      'jobtitle' : [this.storeConfig.job_title, Validators.required],
		      'email': [this.storeConfig.email, [Validators.required, Validators.pattern(emailValid)]],
		      'phone': [this.storeConfig.phone, [Validators.minLength(10)]]
		    });

		    // Load form view do exception not arise
		    this.showForm = true;
	  	})
	  }
	  onSubmitNext(){
		 if(this.userForm.valid){
			/*
			  * hit api here to save data
	          * go to next page on success response
			*/
		  let _value = this.userForm._value; 

			let data = {
						    "shop_id": this.shopId,
						    "shop_info":{
						                "email": _value.email,
						                "name": _value.storename,
						                "domain": _value.storeurl,
						                "phone": _value.phone,
						                "shop_owner": _value.name+" "+_value.lastname,
						                "job_title": _value.jobtitle
						        }
						}
         this.apiService.put('/store/infos',data)
         .subscribe(result=>{
           // alert(result.message);
		   this.router.navigateByUrl('/launcher');
         })

		} else{
		/*
		  * show in case of any error
          * go to next page on success response
		*/	
			return false;
		}
	 }
	 back(){
	 	this.router.navigateByUrl('');
	 }

	 onChange(event:any){
	 	console.log(event);
	 	alert(event)

	 }
}
