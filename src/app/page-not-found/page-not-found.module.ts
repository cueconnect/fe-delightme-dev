import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found.component';
import { SharedModule } from '../shared';

const pageNotFoundRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: '**',
    component: PageNotFoundComponent
  }
]);

@NgModule({
  imports: [
    pageNotFoundRouting,
    SharedModule
  ],
  declarations: [
    PageNotFoundComponent
  ],
  providers: [
  ]
})
export class PageNotFoundModule {}
