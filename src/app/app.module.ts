import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { storeModule } from './store-account/store-account.module';
import { MHHeaderComponent, MHSidebarComponent , HeaderComponent ,SharedModule} from './shared';
import { welcomeModalModule } from './welcome-modal/welcome-modal.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { LauncherModule} from './launcher/launcher.module';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { ApiService } from './api.service';
import { BuildDynamicPricePromotionsModule } from './build-dynamic-price-promotions/build-dynamic-price-promotions.module';
import { BuildDynamicPriceShoppingCartModule } from './build-dynamic-price-shopping-cart/build-dynamic-price-shopping-cart.module';
import { BuildDynamicPriceShoppingAttributeModule } from './build-dynamic-price-shopper-attribute/build-dynamic-price-shopper-attribute.module';
import { CustomizeExperienceHomeModule } from './customize-experience-home/customize-experience-home.module';
import { CustomizeExperienceModule } from './customize-experience/customize-experience.module';
import { CustomizeExperienceLauncherModule } from './customize-experience-launcher/customize-experience-launcher.module';
import { CustomizeExperienceCartModule } from './customize-experience-cart/customize-experience-cart.module';
import { CustomizeExperienceModalsModule } from './customize-experience-modals/customize-experience-modals.module';
import { BuildDynamicPriceHomeModule } from './build-dynamic-price/build-dynamic-price.module';
import { RetargettingModule } from './retargetting/retargetting.module';
import { ReportsModule } from './reports/reports.module';
import { CustomizeExperienceStoreAccountsModule } from './customize-experience-store-account/customize-experience-store-account.module';
import { PriceAndBillingsModule } from './price-and-billings/price-and-billings.module';
import { HelpModule } from './help/help.module';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([]);

@NgModule({
  declarations: [
    AppComponent,
    MHHeaderComponent,
    MHSidebarComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    HomeModule,
    storeModule,
    rootRouting,
    SharedModule,
    welcomeModalModule,
    SidebarModule,
    LauncherModule,
    BuildDynamicPriceHomeModule,
    BuildDynamicPricePromotionsModule,
    RetargettingModule,
    ReportsModule,
    PriceAndBillingsModule,
    HelpModule,
    CustomizeExperienceStoreAccountsModule,
    CustomizeExperienceModalsModule,
    CustomizeExperienceLauncherModule,
    CustomizeExperienceHomeModule,
    CustomizeExperienceCartModule,
    CustomizeExperienceModule,
    BuildDynamicPriceShoppingCartModule,
    BuildDynamicPriceShoppingAttributeModule,
    PageNotFoundModule
  ],
  providers: [
  ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
