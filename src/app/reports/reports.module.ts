import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ReportsComponent } from './reports.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const reportsRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/reports',
    component: ReportsComponent
  }
]);

@NgModule({
  imports: [
    reportsRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    ReportsComponent
  ],
  providers: [
  ]
})
export class ReportsModule {}
