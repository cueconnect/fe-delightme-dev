import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private apiService: ApiService,
    private activeRoute: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    if(this.activeRoute.snapshot.queryParams['shop']){
      let shop_id = (this.activeRoute.snapshot.queryParams['shop']).split(".")[0];
        window.localStorage.setItem('shop_id',shop_id);
      }
  }

  termsAccepted: boolean = true;
  showErrTooltip: boolean = false;

  validateStep(): boolean {
    this.showErrTooltip = false;
    if ( this.termsAccepted ) {
      this.router.navigateByUrl('/store');
      return true;
    }
    this.showErrTooltip = true;
    return false;
  }

}
