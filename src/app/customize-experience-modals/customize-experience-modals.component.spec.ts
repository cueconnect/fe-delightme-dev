import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostomizeExperienceModalsComponent } from './costomize-experience-modals.component';

describe('CostomizeExperienceModalsComponent', () => {
  let component: CostomizeExperienceModalsComponent;
  let fixture: ComponentFixture<CostomizeExperienceModalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostomizeExperienceModalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostomizeExperienceModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
