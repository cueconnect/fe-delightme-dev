import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomizeExperienceModalsComponent } from './customize-experience-modals.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const customizeExperienceModalsRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/modals',
    component: CustomizeExperienceModalsComponent
  }
]);

@NgModule({
  imports: [
    customizeExperienceModalsRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    CustomizeExperienceModalsComponent
  ],
  providers: [
  ]
})
export class CustomizeExperienceModalsModule {}
