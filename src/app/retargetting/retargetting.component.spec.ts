import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetargettingComponent } from './retargetting.component';

describe('RetargettingComponent', () => {
  let component: RetargettingComponent;
  let fixture: ComponentFixture<RetargettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetargettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetargettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
