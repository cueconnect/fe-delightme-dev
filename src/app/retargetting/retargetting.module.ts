import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RetargettingComponent } from './retargetting.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const retargettingRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/retargetting',
    component: RetargettingComponent
  }
]);

@NgModule({
  imports: [
    retargettingRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    RetargettingComponent
  ],
  providers: [
  ]
})
export class RetargettingModule {}
