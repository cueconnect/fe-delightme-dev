import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildDynamicPriceShoppingCartComponent } from './build-dynamic-price-shopping-cart.component';

describe('BuildDynamicPriceShoppingCartComponent', () => {
  let component: BuildDynamicPriceShoppingCartComponent;
  let fixture: ComponentFixture<BuildDynamicPriceShoppingCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildDynamicPriceShoppingCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildDynamicPriceShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
