import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BuildDynamicPriceShoppingCartComponent } from './build-dynamic-price-shopping-cart.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const buildDynamicPriceShoppingCartRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'build-dyanmic-price/shopping-cart',
    component: BuildDynamicPriceShoppingCartComponent
  }
]);

@NgModule({
  imports: [
    buildDynamicPriceShoppingCartRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    BuildDynamicPriceShoppingCartComponent
  ],
  providers: [
  ]
})
export class BuildDynamicPriceShoppingCartModule {}
