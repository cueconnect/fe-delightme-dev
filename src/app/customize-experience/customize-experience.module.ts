import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomizeExperienceComponent } from './customize-experience.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const customizeExperienceRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence',
    component: CustomizeExperienceComponent
  }
]);

@NgModule({
  imports: [
    customizeExperienceRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    CustomizeExperienceComponent
  ],
  providers: [
  ]
})
export class CustomizeExperienceModule {}
