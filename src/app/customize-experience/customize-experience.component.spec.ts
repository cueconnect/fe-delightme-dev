import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostomizeExperienceComponent } from './costomize-experience.component';

describe('CostomizeExperienceComponent', () => {
  let component: CostomizeExperienceComponent;
  let fixture: ComponentFixture<CostomizeExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostomizeExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostomizeExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
