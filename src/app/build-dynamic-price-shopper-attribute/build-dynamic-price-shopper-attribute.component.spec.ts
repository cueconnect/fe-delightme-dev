import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildDynamicPriceShopperAttributeComponent } from './build-dynamic-price-shopper-attribute.component';

describe('BuildDynamicPriceShopperAttributeComponent', () => {
  let component: BuildDynamicPriceShopperAttributeComponent;
  let fixture: ComponentFixture<BuildDynamicPriceShopperAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildDynamicPriceShopperAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildDynamicPriceShopperAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
