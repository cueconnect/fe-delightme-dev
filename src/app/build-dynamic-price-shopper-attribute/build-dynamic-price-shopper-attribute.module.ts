import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BuildDynamicPriceShopperAttributeComponent } from './build-dynamic-price-shopper-attribute.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const buildDynamicPriceShoppingAttributeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'build-dyanmic-price/attribute',
    component: BuildDynamicPriceShopperAttributeComponent
  }
]);

@NgModule({
  imports: [
    buildDynamicPriceShoppingAttributeRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    BuildDynamicPriceShopperAttributeComponent
  ],
  providers: [
  ]
})
export class BuildDynamicPriceShoppingAttributeModule {}
