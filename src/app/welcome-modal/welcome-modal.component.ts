import { Component, OnInit } from '@angular/core';
import {FormsModule,FormGroup, FormControl,Validators,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { ColorPickerInterface } from '../shared/color-picker/color-picker.interface';

@Component({
  selector: 'app-welcome-modal',
  templateUrl: './welcome-modal.component.html',
  styleUrls: ['./welcome-modal.component.scss']
})
export class WelcomeModalComponent implements OnInit {
		welcomeModal: any;
		remindModal: any;
		exitModal: any;
		remindshow:boolean = false
        exitshow:boolean = false;
        welcomeShow:boolean = false;
        welcometext:boolean=false;
        remindtext:boolean=false;
        exittext:boolean=false;
        shopId:string = "qa-delightme";
        welcomeModalVale:any = {
		copyText :"",
        title : "",
        button : ""        	
        }
        remindModalValue:any = {
           title:"",
           button: "",
           copyText : ""
        }
        exitModalValue:any = {
           title:"",
           button: "",
           copyText : ""
        }
        ColorPickerInterface: ColorPickerInterface = {
		    color: '#999999'
		  }
  constructor(private formBuilder: FormBuilder,
		  	private router: Router,
  	        private apiService: ApiService
		  	) { 
               }

  ngOnInit() {
  	if(window.localStorage.getItem('shop_id')){
          this.shopId = window.localStorage.getItem('shop_id');
        }
  	this.loadDefaultConfiguration(true);
  }

  onChange(value:any,id:number, modelVlue:string){
  	 if(id==1){
	    document.getElementById('delightme-welcome-content').style.backgroundColor = value;
	    this.updateBgColor(value);       
  	 }
  	 if(id==2){
	     document.getElementById('welcome-submit').style.backgroundColor = value;
         this.updateActiveColor(value);
  	 }
  	if(modelVlue=='welcomeModal'){  		
	  	if(id==1){
	  	}
	  	else if(id==2){
	  	}
	  	else if(id==3){
	     this.welcomeModalVale.title = value;
	  	} else if(id==4){
	     this.welcomeModalVale.button = value;
	  	} else{
	     this.welcomeModalVale.copyText = value;
	  	}
  	} else if(modelVlue=='remindModal'){
        if(id==1){
	  	}
	  	else if(id==2){
	  	}
	  	else if(id==3){
	     this.remindModalValue.title = value;
	  	} else if(id==4){
	     this.remindModalValue.button = value;
	  	} else{
	     this.remindModalValue.copyText = value;
	  	}
  	} else {
        if(id==1){
	  	}
	  	else if(id==2){
	  	}
	  	else if(id==3){
	     this.exitModalValue.title = value;
	  	} else if(id==4){
	     this.exitModalValue.button = value;
	  	} else{
	     this.exitModalValue.copyText = value;
	  	}
  	}
  }

   // form submission 

  onSubmitForm(id:number,value:boolean){
  	if(!value){  		
	  	if(id==1){
	  		this.reminder(true)
	  	}
	  	else if(id==2){
	  		this.exit(true)
	  	} 
	  	else{
	  		let welcomeValue = this.welcomeModal._value; 
	  		let remindValue = this.remindModal._value; 
	  		let exitValue = this.exitModal._value; 
	  		let data = {
				    "shop_id": this.shopId,
				    "modals": {
				        "welcome": {
				            "button": welcomeValue.button,
				            "bg_color": welcomeValue.backgroundColor,
				            "visible_on_mobile":welcomeValue.visibleOnMobile,
				            "active_color": welcomeValue.activeColor,
				            "visible_on_desktop": welcomeValue.visibleOnDesktop,
				            "title": welcomeValue.title,
				            "content": welcomeValue.copytext
				        },
				        "exit": {
				            "button": exitValue.button,
				            "bg_color": exitValue.backgroundColor,
				            "visible_on_mobile": exitValue.visibleOnMobile,
				            "active_color":exitValue.activeColor,
				            "visible_on_desktop":exitValue.visibleOnDesktop,
				            "title": exitValue.title,
				            "content": exitValue.copytext
				        },
				        "remind": {
				            "button": remindValue.button,
				            "bg_color": remindValue.backgroundColor,
				            "visible_on_mobile": remindValue.visibleOnMobile,
				            "active_color": remindValue.activeColor,
				            "visible_on_desktop": remindValue.visibleOnDesktop,
				            "title": remindValue.title,
				            "content": remindValue.copytext
				        }
				    }
				}
				this.apiService.put('/store/modals',data)
		         .subscribe(result=>{	
		             // alert(result.message);	         	
	 		      this.router.navigateByUrl('');	
		         })
	  	}
  	} else{
  		return false;
  	}
  }

  resetDefault(){ 
      this.loadDefaultConfiguration(false);
  }

 back(id:number){
	 	if(id==1){ 		
		 	this.router.navigateByUrl('/sidebar');
	 	}
	 	else if(id==2){ 		
 			this.welcome(true);
	 	}
	 	else{
            this.reminder(true);
	 	}
	 }

 next(){
	 	this.router.navigateByUrl('');
	 }

	 welcome(option:boolean){
        if(option){
        this.welcomeShow=true;
        this.remindshow=false;
        this.exitshow=false;
        document.getElementById('welcomeId').classList.add('active');
 		document.getElementById('reminderId').classList.remove('active');
 		document.getElementById('exitId').classList.remove('active');
 		this.loadFormColor(this.welcomeModal._value.backgroundColor,this.welcomeModal._value.activeColor); 		
        }

	 }
	  reminder(option:boolean){
        if(option){
        this.remindshow=true;
        this.welcomeShow=false;
        this.exitshow=false;
        document.getElementById('welcomeId').classList.remove('active');
        document.getElementById('exitId').classList.remove('active');
 		document.getElementById('reminderId').classList.add('active');
 		this.loadFormColor(this.remindModal._value.backgroundColor,this.remindModal._value.activeColor);
        }

	 }
	  exit(option:boolean){
        if(option){
        this.exitshow=true;
        this.welcomeShow=false;
        this.remindshow=false;
        document.getElementById('welcomeId').classList.remove('active');
 		document.getElementById('reminderId').classList.remove('active');
 		document.getElementById('exitId').classList.add('active');
 		this.loadFormColor(this.exitModal._value.backgroundColor,this.exitModal._value.activeColor);
        }

	 }

	 loadFormColor(bgColor:string, activeColor:string){
	 	setTimeout(function(){
 		document.getElementById('delightme-welcome-content').style.backgroundColor = bgColor;
 		document.getElementById('welcome-submit').style.backgroundColor = activeColor;
 		 }, 0);
	 }


	 loadDefaultConfiguration(option:boolean){
  	  	this.apiService.get('/store?shop_id='+this.shopId)
	  	.subscribe(result =>{

	  		window.localStorage.setItem('authToken',result.data[0].dm_auth_token);
	  		window.localStorage.setItem('shopToken',result.data[0].shop_token);

	  		let storeConfig=result.data[0].shop_config.modals;
             let welcome   =  storeConfig.welcome ; 
             let color = /^#[0-9a-f]{3}([0-9a-f]{3})?$/i;

             this.welcomeModalVale.copyText = welcome.content;
             this.welcomeModalVale.title = welcome.title;
             this.welcomeModalVale.button = welcome.button;

	  		 this.welcomeModal = this.formBuilder.group({
		  	  'backgroundColor': [welcome.bg_color, [ Validators.required,Validators.pattern(color)]],
		  	  'activeColor': [welcome.active_color,[ Validators.required,Validators.pattern(color)]],
		      'visibleOnDesktop': [welcome.visible_on_desktop],
		      'visibleOnMobile': [welcome.visible_on_mobile],
		      'title': [welcome.title,Validators.required],
		      'button' :  [welcome.button,[ Validators.required,Validators.maxLength(30)]],
		      'copytext' : [welcome.content, Validators.required]
		    });

             let remind   =  storeConfig.remind ; 

             this.remindModalValue.copyText = remind.content;
             this.remindModalValue.title = remind.title;
             this.remindModalValue.button = remind.button; 

		    this.remindModal = this.formBuilder.group({
		  	  'backgroundColor': [remind.bg_color, [ Validators.required,Validators.pattern(color)]],
		  	  'activeColor': [remind.active_color, [ Validators.required,Validators.pattern(color)]],
		      'visibleOnDesktop': [remind.visible_on_desktop],
		      'visibleOnMobile': [remind.visible_on_mobile],
		      'title': [remind.title,Validators.required],
		      'button' :  [remind.button,[ Validators.required,Validators.maxLength(30)]],
		      'copytext' : [remind.content, Validators.required]
		    });
 
            let exit   =  storeConfig.exit ; 

             this.exitModalValue.copyText = exit.content;
             this.exitModalValue.title = exit.title;
             this.exitModalValue.button = exit.button; 

		    this.exitModal = this.formBuilder.group({
		  	  'backgroundColor': [exit.bg_color,[ Validators.required,Validators.pattern(color)]],
		  	  'activeColor': [exit.active_color,[ Validators.required,Validators.pattern(color)]],
		      'visibleOnDesktop': [exit.visible_on_desktop],
		      'visibleOnMobile': [exit.visible_on_mobile],
		      'title': [exit.title,Validators.required],
		      'button' :  [exit.button,[ Validators.required,Validators.maxLength(30)]],
		      'copytext' : [exit.content, Validators.required]
		    });
            if(option){
		    this.welcomeShow = true;
            }
            this.loadFormColor(this.welcomeModal._value.backgroundColor,this.welcomeModal._value.activeColor);	
	  	})
	 }
	 welcometextsetting(){
      this.welcometext=!this.welcometext;

	 }
	  remindtextsetting(){
       this.remindtext=!this.remindtext; 
	 
	 }
	  exittextsetting(){
      this.exittext=!this.exittext; 
	 
	 }

	 updateBgColor(value:any){
	    if(this.welcomeShow){
        this.remindModal.controls['backgroundColor'].setValue(value);
        this.exitModal.controls['backgroundColor'].setValue(value);
	     }
	    if(this.remindshow){
		  	this.welcomeModal.controls['backgroundColor'].setValue(value);
	        this.exitModal.controls['backgroundColor'].setValue(value);
		  }
		if(this.exit){
	      this.welcomeModal.controls['backgroundColor'].setValue(value);
	      this.remindModal.controls['backgroundColor'].setValue(value);
		  }
	}

	updateActiveColor(value:any){
	    if(this.welcomeShow){
        this.remindModal.controls['activeColor'].setValue(value);
        this.exitModal.controls['activeColor'].setValue(value);
	     }
	    if(this.remindshow){
		  	this.welcomeModal.controls['activeColor'].setValue(value);
	        this.exitModal.controls['activeColor'].setValue(value);
		  }
		if(this.exit){
	      this.welcomeModal.controls['activeColor'].setValue(value);
	      this.remindModal.controls['activeColor'].setValue(value);
		  }
	}
}
