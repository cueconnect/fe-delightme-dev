import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { WelcomeModalComponent } from './welcome-modal.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const welcomeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'welcome-modal',
    component: WelcomeModalComponent
  }
]);

@NgModule({
  imports: [
    welcomeRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    WelcomeModalComponent
  ],
  providers: [
  ]
})
export class welcomeModalModule {}