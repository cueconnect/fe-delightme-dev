import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomizeExperienceStoreAccountComponent } from './customize-experience-store-account.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const customizeExperienceStoreAccountRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/store-account',
    component: CustomizeExperienceStoreAccountComponent
  }
]);

@NgModule({
  imports: [
    customizeExperienceStoreAccountRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    CustomizeExperienceStoreAccountComponent
  ],
  providers: [
  ]
})
export class CustomizeExperienceStoreAccountsModule {}
