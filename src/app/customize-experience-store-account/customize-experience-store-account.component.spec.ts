import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostomizeExperienceStoreAccountComponent } from './costomize-experience-store-account.component';

describe('CostomizeExperienceStoreAccountComponent', () => {
  let component: CostomizeExperienceStoreAccountComponent;
  let fixture: ComponentFixture<CostomizeExperienceStoreAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostomizeExperienceStoreAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostomizeExperienceStoreAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
