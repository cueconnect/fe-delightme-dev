import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomizeExperienceHomeComponent } from './customize-experience-home.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const customizeExperienceHomeRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/home',
    component: CustomizeExperienceHomeComponent
  }
]);

@NgModule({
  imports: [
    customizeExperienceHomeRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    CustomizeExperienceHomeComponent
  ],
  providers: [
  ]
})
export class CustomizeExperienceHomeModule {}
