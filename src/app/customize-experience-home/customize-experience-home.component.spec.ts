import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostomizeExperienceHomeComponent } from './costomize-experience-home.component';

describe('CostomizeExperienceHomeComponent', () => {
  let component: CostomizeExperienceHomeComponent;
  let fixture: ComponentFixture<CostomizeExperienceHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostomizeExperienceHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostomizeExperienceHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
