import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HelpComponent } from './help.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const helpRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/help',
    component: HelpComponent
  }
]);

@NgModule({
  imports: [
    helpRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    HelpComponent
  ],
  providers: [
  ]
})
export class HelpModule {}
