import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl,Validators,FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { ColorPickerInterface } from '../shared/color-picker/color-picker.interface';

@Component({
  selector: 'app-launcher',
  templateUrl: './launcher.component.html',
  styleUrls: ['./launcher.component.scss']
})
export class LauncherComponent implements OnInit {
		unlockSecretPrice: any;
		myTotalSaving:any;
		UnlockText:string='';
		UnlockLowerText:string='';
		SavingText:string='';
		upText:string = "Unlock";
		lowwerText:string = "Price";
        unlocksecret:boolean=false;
        myTotalsaving:boolean=false;
        primaryColor:string="";
		svgColor:string = "";
		unlockSecretPricetext:any;
		hoverColor:string = "";
		unlockPrice:boolean = false;
        totalSaving:boolean = false;
        storeConfig:any;
        shopId:string = "qa-delightme";
        edgeValue: number;

  constructor(
  	private formBuilder: FormBuilder,
  	private router: Router,
  	private apiService: ApiService
  	) {
    }

  ColorPickerInterface: ColorPickerInterface = {
    color: '#999999'
  }

  setEdge(num: number): void {
    this.edgeValue = num;
  }

  ngOnInit() {
     
     if(window.localStorage.getItem('shop_id')){
          this.shopId = window.localStorage.getItem('shop_id');
        }
   this.loadDefaultConfiguration(true);
  }
  

 unlockBack(id:number){
      if(id==1){
	 	this.router.navigateByUrl('/store');
	 	}else{
	 	this.unlockPrice=true;
        this.totalSaving=false;
	 	document.getElementById('unlocksecret').style.display='block';
 		document.getElementById('mytotalsaving').style.display='none';
 		document.getElementById('myPriceli').classList.remove('active');
 		document.getElementById('Unlockli').classList.add('active');
	 	}
	 }

 myTotalNext(id:number){
 	if(!this.unlockSecretPrice.valid || !this.myTotalSaving.valid){
     return false;
 	} else { 		
     if(id==2){
	 	let _valueUnlock = this.unlockSecretPrice._value;
 	   let _valueTotal = this.myTotalSaving._value;
        let data = {
			    "shop_id": this.shopId,
			    "launcher": {
			        "hover_coler": _valueTotal.hovercolor,
			        "saving_text":_valueTotal.copytext,
			        "position": _valueTotal.position,
			        "primary_color": _valueTotal.primarycolor,
			        "edge_px": _valueTotal.edge,
			        "unlock_text": _valueUnlock.copytext,
			        "font": _valueTotal.fontface
			    }
			}
		this.apiService.put('/store/launcher',data)
         .subscribe(result=>{
         	// alert(result.message);
	 	  this.router.navigateByUrl('/sidebar');
         })
	 	}else{
	 	 this.totalSaving=true;
 	     this.unlockPrice=false;
 		document.getElementById('unlocksecret').style.display='none';
 		document.getElementById('mytotalsaving').style.display='block';
 		document.getElementById('Unlockli').classList.remove('active');
 		document.getElementById('myPriceli').classList.add('active');
	 	}
 	}
	 }
 onChange(event:any){
  document.getElementById('launcher').className = event;
  if(this.unlockPrice){  	
  this.myTotalSaving.controls['position'].setValue(event);
  } else {  	
  this.unlockSecretPrice.controls['position'].setValue(event);
  }
 }
 unlockForm(option:boolean){
 	if(option){
        this.unlockPrice=true;
        this.totalSaving=false;
 		this.upText = "Unlock";
 		this.lowwerText = "Price";
 		document.getElementById('unlocksecret').style.display='block';
 		document.getElementById('mytotalsaving').style.display='none';
 		document.getElementById('myPriceli').classList.remove('active');
 		document.getElementById('Unlockli').classList.add('active');
 	} else {
 	    this.totalSaving=true;
 	     this.unlockPrice=false;
 		this.upText = "My total";
 		this.lowwerText = "Saving";
 		document.getElementById('unlocksecret').style.display='none';
 		document.getElementById('mytotalsaving').style.display='block';
 		document.getElementById('Unlockli').classList.remove('active');
 		document.getElementById('myPriceli').classList.add('active');
 	}
 }
 onChangePrimaryColor(event:any){
       this.svgColor =  event;
       if(this.unlockPrice){  	
		  this.myTotalSaving.controls['primarycolor'].setValue(event);
		  } else {  	
		  this.unlockSecretPrice.controls['primarycolor'].setValue(event);
		}
 }
 onChangeHoverColor(event:any){
       this.hoverColor =  event;
       if(this.unlockPrice){  	
		  this.myTotalSaving.controls['hovercolor'].setValue(event);
		  } else {  	
		  this.unlockSecretPrice.controls['hovercolor'].setValue(event);
		}
 }
 over(){
 	this.svgColor = this.hoverColor;
 }
 mouseLeave(){
 	this.svgColor = this.myTotalSaving._value.primarycolor; 
 }
textsetting(){
	this.unlocksecret=!this.unlocksecret;
}
savingtextsetting(){
	this.myTotalsaving=!this.myTotalsaving;
}
onChangeUnlockText(value:any , id:number){
	if(id==1){
 		 this.UnlockText = value;
 	}
}
onChangeSavingText(value:any , id:number){
	if(id==1){
 		 this.SavingText = value;
 	}
}

resetDefault(){ 
      this.loadDefaultConfiguration(false);
  }
loadDefaultConfiguration(value:boolean){

    /*
      * Api service to getting store configurtion  
    */

	  	this.apiService.get('/store?shop_id='+this.shopId)
	  	.subscribe(result =>{
	  		this.storeConfig=result.data[0].shop_config.launcher;
	  		window.localStorage.setItem('authToken',result.data[0].dm_auth_token);
	  		window.localStorage.setItem('shopToken',result.data[0].shop_token);
	  		this.storeConfig=result.data[0].shop_config.launcher;
	  		this.hoverColor=this.storeConfig.hover_coler;
	  		this.svgColor=this.storeConfig.primary_color;
             
             let color = /^#[0-9a-f]{3}([0-9a-f]{3})?$/i;
            this.unlockSecretPricetext=this.storeConfig.unlock_text.split(" ")
            this.UnlockText =this.unlockSecretPricetext[0];
            for(var i=1;i<this.unlockSecretPricetext.length;i++){
               this.UnlockLowerText = this.UnlockLowerText + " "+this.unlockSecretPricetext[i];
            }
	  		this.primaryColor=this.storeConfig.primary_color;
	  		this.SavingText=this.storeConfig.saving_text;
	  		this.unlockSecretPrice = this.formBuilder.group({
		  	  'position': [this.storeConfig.position],
		  	  'fontface': [this.storeConfig.font, Validators.required],
		      'edge': [this.storeConfig.edge_px, Validators.required],
		      'primarycolor' :  [this.storeConfig.primary_color,[ Validators.required,Validators.pattern(color)]],
		      'hovercolor' : [this.storeConfig.hover_coler, [Validators.required,,Validators.pattern(color)]],
		      'copytext': [this.storeConfig.unlock_text, [Validators.required]]
		    });

		    this.myTotalSaving = this.formBuilder.group({
		  	  'position': [this.storeConfig.position, Validators.required],
		  	  'fontface': [this.storeConfig.font, Validators.required],
		      'edge': [this.storeConfig.edge_px,Validators.required],
		      'primarycolor' :  [this.storeConfig.primary_color,[Validators.required,Validators.pattern(color)]],
		      'hovercolor' : [this.storeConfig.hover_coler,[Validators.required, Validators.pattern(color)]],
		      'copytext': [this.storeConfig.saving_text, [Validators.required]]
		    });
             this.edgeValue = this.storeConfig.edge_px;
		      if(value){
		      this.unlockPrice = true;		      	
		      }
  			  document.getElementById('launcher').className = this.storeConfig.position;
	  	})	
}
}
