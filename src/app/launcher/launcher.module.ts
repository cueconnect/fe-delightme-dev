import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LauncherComponent } from './launcher.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';


const launcherRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'launcher',
    component: LauncherComponent
  }
]);

@NgModule({
  imports: [
    launcherRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    LauncherComponent
  ],
  providers: [
  ]
})
export class LauncherModule {}
