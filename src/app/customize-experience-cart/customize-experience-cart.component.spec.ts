import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostomizeExperienceCartComponent } from './costomize-experience-cart.component';

describe('CostomizeExperienceCartComponent', () => {
  let component: CostomizeExperienceCartComponent;
  let fixture: ComponentFixture<CostomizeExperienceCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostomizeExperienceCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostomizeExperienceCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
