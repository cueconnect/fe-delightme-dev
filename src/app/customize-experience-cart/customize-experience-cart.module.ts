import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomizeExperienceCartComponent } from './customize-experience-cart.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const CustomizeExperienceCartRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/reports',
    component: CustomizeExperienceCartComponent
  }
]);

@NgModule({
  imports: [
    CustomizeExperienceCartRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    CustomizeExperienceCartComponent
  ],
  providers: [
  ]
})
export class CustomizeExperienceCartModule {}
