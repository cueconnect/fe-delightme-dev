import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PriceAndBillingsComponent } from './price-and-billings.component';
import { SharedModule } from '../shared';
import { ColorPickerModule } from '../shared/color-picker';

const priceAndBillingsRouting: ModuleWithProviders = RouterModule.forChild([
  {
    path: 'customize-expirence/priceAndBillings',
    component: PriceAndBillingsComponent
  }
]);

@NgModule({
  imports: [
    priceAndBillingsRouting,
    SharedModule,
    ColorPickerModule
  ],
  declarations: [
    PriceAndBillingsComponent
  ],
  providers: [
  ]
})
export class PriceAndBillingsModule {}
