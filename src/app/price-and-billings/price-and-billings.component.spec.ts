import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceAndBillingsComponent } from './price-and-billings.component';

describe('PriceAndBillingsComponent', () => {
  let component: PriceAndBillingsComponent;
  let fixture: ComponentFixture<PriceAndBillingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PriceAndBillingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceAndBillingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
