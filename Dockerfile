# Use phusion/baseimage as base image. 
# Baseimage-docker is very lightweight: it only consumes 6 MB of memory.
# ubuntu 16.04
# syslog-ng version: 3.5.6
FROM phusion/baseimage:0.9.18

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]
# install node7
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y build-essential
RUN apt-get install -y nodejs
RUN apt-get update -y
RUN apt-get install -y apache2
RUN a2enmod ssl

RUN npm -g config set user root
# add a user
WORKDIR /home

# instructions for delightme project
ENV APP_DIR /home
WORKDIR $APP_DIR
COPY . $APP_DIR
COPY apache2 /etc/apache2
RUN chown -R $(whoami) /usr/lib/node_modules
RUN chown -R $(whoami) /usr/bin

RUN npm install -g @angular/cli
RUN cd /home

RUN npm install
RUN npm rebuild node-sass --force
RUN ng build 

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
